## Projet_Dactylographie :

Le but de ce projet est de développer en C un jeu de dactylographie permettant donc de s'entraîner à taper au clavier.

Actuellement en cours de développement il devra à terme avoir les caractéristiques suivantes :

*  permettre au joueur de s'entraîner à taper un maximum de mots en une minute
*  permettre au joueur de sélectionner un mode dictée (il entendra un enregistrement audio qu'il devra taper au clavier dans les temps et sans fautes)
*  ce jeu devra donc posséder une interface graphique
*  compatible windows et linux
*  tableau des meilleurs scores
*  création d'un compte joueur
*  les mots erronés seront enregistrés et le joueur aura la possibilité de choisir de s'entraîner sur ces mots
*  




